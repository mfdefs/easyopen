package com.gitee.easyopen;

import java.security.interfaces.RSAPrivateKey;

/**
 * 负责加解密
 * @author tanghc
 *
 */
public interface Encrypter {
    /**
     * 获取私钥对象
     * @param priKey
     * @return
     * @throws Exception 
     */
    RSAPrivateKey getPrivateKey(String priKey) throws Exception;
    
    /**
     * AES文本加密
     * @param content 明文
     * @param password 密码
     * @return 返回16进制内容
     * @throws Exception
     */
    String aesEncryptToHex(String content, String password) throws Exception;
    
    /**
     * AES文本解密
     * @param hex 待解密文本,16进制内容
     * @param password 密码
     * @return 返回明文
     * @throws Exception
     */
    String aesDecryptFromHex(String hex, String password) throws Exception;
    
    /**
     * RSA私钥解密
     * 
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     */
     String rsaDecryptByPrivateKey(String data, RSAPrivateKey privateKey) throws Exception;
     
     /**
      * RSA私钥加密
      * 
      * @param data
      * @param publicKey
      * @return
      * @throws Exception
      */
     String rsaEncryptByPrivateKey(String data, RSAPrivateKey privateKey) throws Exception ;
    
     /**
      * md5加密,全部小写
      * @param value
      * @return
      */
     String md5(String value);
}
