package com.gitee.easyopen;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理请求
 * @author tanghc
 *
 */
public interface Invoker extends HasConfig {
    
    /**
     * 调用接口方法
     * 
     * @param request
     * @param response
     * @return
     */
    void invoke(HttpServletRequest request, HttpServletResponse response);
   
    /**
     * 写数据到客户端
     * 
     * @param response
     * @param result 结果
     */
    void caugthException(HttpServletResponse response, Throwable e);
}
