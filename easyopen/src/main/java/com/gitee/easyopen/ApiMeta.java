package com.gitee.easyopen;

import java.lang.reflect.Method;

public interface ApiMeta {
    Object getHandler();

    Method getMethod();

    Class<?> getMethodArguClass();

    String getName();

    String getVersion();

    boolean isIgnoreSign();

    boolean isIgnoreValidate();

    boolean isWrapResult();
}
