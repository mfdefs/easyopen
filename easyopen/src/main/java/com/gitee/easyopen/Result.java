package com.gitee.easyopen;

import java.io.Serializable;

public interface Result extends Serializable{
    void setCode(Object code);

    void setMsg(String msg);

    void setData(Object data);
}
