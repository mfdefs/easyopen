package com.gitee.easyopen.server.auth;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.auth.AccessToken;
import com.gitee.easyopen.auth.Oauth2Manager;
import com.gitee.easyopen.auth.OpenUser;
import com.gitee.easyopen.exception.LoginErrorException;
import com.gitee.easyopen.message.Errors;
import com.gitee.easyopen.server.model.User;

/**
 * oauth2认证实现类
 * @author tanghc
 *
 */
@Service
public class Oauth2ManagerImpl implements Oauth2Manager {
    /****下面是缓存，最好用redis代替*****/
    // 存放用户对应的code,key=code,value=openUser
    private static Map<String, OpenUser> userCodeMap = new HashMap<>();
    // 存放用户对应的accessToken
    private static Map<String, AccessToken> userAccessTokenMap = new HashMap<>();

    @Override
    public void addAuthCode(String authCode, OpenUser openUser) {
        userCodeMap.put(authCode, openUser);
    }

    @Override
    public void addAccessToken(String accessToken, OpenUser openUser, long expiresIn) {
        userAccessTokenMap.put(accessToken, new AccessToken(expiresIn, openUser));
    }

    @Override
    public boolean checkAuthCode(String authCode) {
        if(StringUtils.isEmpty(authCode)) {
            return false;
        }
        return userCodeMap.keySet().contains(authCode);
    }

    @Override
    public OpenUser getUserByAuthCode(String authCode) {
        return userCodeMap.get(authCode);
    }

    @Override
    public OpenUser getUserByAccessToken(String accessToken) {
        if(accessToken == null) {
            throw Errors.ERROR_ACCESS_TOKEN.getException();
        }
        AccessToken accessTokenInfo = userAccessTokenMap.get(accessToken);
        
        if(accessTokenInfo == null) {
            throw Errors.ERROR_ACCESS_TOKEN.getException();
        }
        
        if(accessTokenInfo.isExpired()) {
            userAccessTokenMap.remove(accessToken);
            throw Errors.EXPIRED_ACCESS_TOKEN.getException();
        }
        return accessTokenInfo.getOpenUser();
    }

    @Override
    public long getExpireIn(ApiConfig apiConfig) {
        return apiConfig.getOauth2ExpireIn();
    }

    @Override
    public OpenUser login(HttpServletRequest request) throws LoginErrorException {
        // 这里应该先检查用户有没有登录，如果登录直接返回openUser
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new LoginErrorException("用户名密码不能为空");
        }
        // 此处应该从数据库中查
        if(!("admin".equals(username) && "123456".equals(password))) {
            throw new LoginErrorException("用户名密码不正确");
        }
        // 模拟登录..
        
        // 登录成功
        User user = new User();
        user.setId(1L);
        user.setUsername(username);
        user.setPassword(password);
        
        return user;
    }

}
