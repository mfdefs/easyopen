package com.gitee.easyopen.sdk;

/**
 * 数据加密传输客户端
 * 
 * @author tanghc
 *
 */
public class EncryptClient extends OpenClient {

    public EncryptClient(String url, String appKey) {
        super(url, appKey, "");
        this.setRequestMode(RequestMode.ENCRYPT);
    }

    private EncryptClient(String url, String appKey, String secret) {
        super(url, appKey, secret);
        this.setRequestMode(RequestMode.ENCRYPT);
    }

    private EncryptClient(String url, String appKey, String secret, String lang) {
        super(url, appKey, secret, lang);
        this.setRequestMode(RequestMode.ENCRYPT);
    }
    
    @Override
    public void setRequestMode(RequestMode requestMode) {
        super.setRequestMode(RequestMode.ENCRYPT);
    }

}
