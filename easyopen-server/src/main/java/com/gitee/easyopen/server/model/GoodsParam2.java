package com.gitee.easyopen.server.model;

import com.gitee.easyopen.doc.DataType;
import com.gitee.easyopen.doc.annotation.ApiDocField;

public class GoodsParam2 extends GoodsParam {

    @ApiDocField(description="商品价格",dataType=DataType.FLOAT)
    private double goodsPrice;

    public double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    
}
