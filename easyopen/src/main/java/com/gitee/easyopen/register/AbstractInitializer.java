package com.gitee.easyopen.register;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.ApiContext;

public abstract class AbstractInitializer implements Initializer {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    private static boolean inited = false;

    @Override
    public synchronized void init(ApplicationContext applicationContext, ApiConfig apiConfig) {
        if (!inited) {
            Assert.notNull(applicationContext, "applicationContext不能为null");
            Assert.notNull(apiConfig, "apiConfig不能为null");

            try {
                new ApiRegister(apiConfig, applicationContext).regist();

                ApiContext.setApiConfig(apiConfig);
                ApiContext.setApplicationContext(applicationContext);
                inited = true;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                System.exit(0);
            }
        }
    }

}
