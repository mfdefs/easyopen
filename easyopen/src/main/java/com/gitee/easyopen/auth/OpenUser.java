package com.gitee.easyopen.auth;

import java.io.Serializable;

public interface OpenUser extends Serializable {
    String getUsername();
}
